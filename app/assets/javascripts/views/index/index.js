Public.Index = function($wrapper) {
  this.$wrapper = $wrapper;
  this.initialized = false;
  this.state = { oldMutedState: true };
  this.scrollClick = new Components.ScrollClick('.js-play');
  this.game = new Game.Executor($wrapper.find(".js-game"));
  this.game.init();
};

Public.Index.prototype.init = function() {
  this.initialized = true;
  this._listenToControls();
  $(".js-pause-control").fadeOut(0);
};

Public.Index.prototype.build = function(newState) {
  if (this.initialized) {
    $.extend(this.state, newState);
    this._render();
  }
};

Public.Index.prototype._render = function() {
  this.scrollClick.build();
};

Public.Index.prototype._listenToControls = function() {
  this._listenToRestart();
  this._listenToPause();
  this._listenToMusic();
};

Public.Index.prototype._listenToRestart = function() {
  var self = this;

  $("body").on("click", ".js-start", function(event) {
    event.preventDefault();
    self.game = new Game.Executor(self.$wrapper.find(".js-game"));
    self.game.init();
    self.game.build({
      paused: false,
      gameOver: false,
      gameSpeed: 100,
      muted: !!self.state.oldMutedState
    });
    self.game.removeGameScreenProtector();
    $(".js-pause-control").fadeIn(0);
  });
};

Public.Index.prototype._listenToPause = function() {
  var self = this;

  $("body").on("click", ".js-pause-control", function(event) {
    event.preventDefault();
    self._toggleGamePauseState($(this));
  });

  $(document).keydown(function(event){
    var selectedKey = event.which;

    if(selectedKey == 32 || selectedKey ==  27) {
      event.preventDefault();
      self._toggleGamePauseState($(".js-pause-control"));
    }
  });
};

Public.Index.prototype._toggleGamePauseState = function($button) {
  if (!$button.hasClass("paused")) {
    this.state.oldMutedState = !!this.game.state.muted;
    this.game.build({paused: true, muted: true});
    $button.removeClass("fa-pause").addClass("fa-play paused");
    $("[data-game-sound]")[0].pause();
  }
  else {
    this.game.build({paused: false, muted: this.state.oldMutedState});
    if (!this.state.oldMutedState) { $("[data-game-sound]")[0].play(); }
    $button.removeClass("fa-play paused").addClass("fa-pause");
  }
};

Public.Index.prototype._listenToMusic = function() {
  var self = this;

  $("body").on("click", ".js-music-control", function(event) {
    event.preventDefault();
    self._toggleGameMusicState($(this));
  });
};

Public.Index.prototype._toggleGameMusicState = function($button) {
  if (!$button.hasClass("muted")) {
    this.state.oldMutedState = true;
    this.game.updateState({muted: true});
    $button.removeClass("fa-volume-up").addClass("fa-volume-off muted");
    $("[data-game-sound]")[0].pause();
  }
  else {
    this.state.oldMutedState = false;
    this.game.updateState({muted: false});
    $button.removeClass("fa-volume-off muted").addClass("fa-volume-up");
    $("[data-game-sound]")[0].play();
  }
};