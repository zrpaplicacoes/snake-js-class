Game.Position = function(x, y) {
  this.GAMEPLAYSIZE = Game.Executor.gameboardSize();
  this.state = {};
  this.state.x = Math.floor(Math.random() * (this.GAMEPLAYSIZE - 1));
  this.state.y = Math.floor(Math.random() * (this.GAMEPLAYSIZE - 1));
  this.state.direction = "right";
};

Game.Position.prototype.setPosition = function(x, y) {
  if (x >= this.GAMEPLAYSIZE) {
    x = 0;
  }
  if (y >= this.GAMEPLAYSIZE) {
    y = 0;
  }
  if (x < 0) {
    x = this.GAMEPLAYSIZE - 1;
  }
  if (y < 0) {
    y = this.GAMEPLAYSIZE - 1;
  }

  this.state.x = x;
  this.state.y = y;
};

Game.Position.prototype.set1DPosition = function(index) {
  this.state.x = index % this.GAMEPLAYSIZE;
  this.state.y = Math.floor(index / this.GAMEPLAYSIZE);
};

Game.Position.prototype.copyPosition = function(position) {
  this.state.x = position.state.x;
  this.state.y = position.state.y;
};

Game.Position.prototype.distanceFrom = function(position) {
  return [this.state.x - position.state.x, this.state.y - position.state.y];
};

Game.Position.prototype.getElement = function() {
  return Game.Executor.getWrapper().find(".pixel:nth-child(" + this._get1DPosition() + ")");
};

Game.Position.prototype._get1DPosition = function() {
  return this.state.y * this.GAMEPLAYSIZE + this.state.x + 1;
};
