Game.Engine.LevelController = function() {
  this.state = {}
};

Game.Engine.LevelController.prototype.build = function(newState) {
  this.updateState(newState);
  this._renderLevel();
};

Game.Engine.LevelController.prototype.updateState = function(gameState) {
  updateState({
    state: this.state,
    newState: gameState
  });
};

Game.Engine.LevelController.prototype.increaseScoreBy = function(inscreasedAmount) {
  this.state.oldLevel = this.state.level;
  this.state.score += inscreasedAmount;
  this.state.level = Math.ceil(this.state.score / 500);
  this._updateGameSpeedBasedOnLevel();
};

Game.Engine.LevelController.prototype._updateGameSpeedBasedOnLevel = function() {
  if ((this.state.level - this.state.oldLevel) >= 1) {
    if (this.state.gameSpeed > 20) {
      this.state.gameSpeed -= 20;
    }

    if (this.state.level > 5) {
      this.state.map.setLevel(3);
    }
    else {
      this.state.map.setLevel(this.state.level);
    }

    this.state.soundController.levelRaise();
  }
};

Game.Engine.LevelController.prototype._renderLevel = function() {
  $(".js-score").text(this.state.score);
  $(".js-level").text(this.state.level);
};