Game.Engine.ElementsGenerator = function() {
  this.state = {};
};

Game.Engine.ElementsGenerator.prototype.init = function() {
  this._initializeFood();
  this._initializeSnake();
  this._initializeMap();
  this._initializeSkill();
};

Game.Engine.ElementsGenerator.prototype.build = function(newState) {
  this.updateState(newState);
  if (this.state.food.endedUptime()) {
    this.generateNewFood();
  }
};

Game.Engine.ElementsGenerator.prototype.updateState = function(gameState) {
  updateState({
    state: this.state,
    newState: gameState
  });
}

Game.Engine.ElementsGenerator.prototype.generateNewFood = function() {
  if (this.state.skill.skin == "magnetized") {
    this.state.food = new Game.Food.Basic();
    this.state.alreadyMagnetizedFood = false;
  }
  else if (this.state.level == 2) {
    this._getRandomicFood(["magnetize", "uctt"]);
  }
  else if (this.state.level > 2) {
    this._getRandomicFood(["uctt"]);
  }
  else if (this.state.level > 4) {
    this._getRandomicFood();
  }
  else {
    this._getRandomicFood(["magnetize", "shield", "uctt"]);
  }
};

Game.Engine.ElementsGenerator.prototype.updateSkillWithFood = function() {
  if (this.state.skill.skin != "magnetized") {
    this.state.skill = this.state.food.skill;
    this.state.lastSkilledEatenFood = this.state.food;
  }
  else {
    if (this.state.skill.endedUptime()) {
      this.state.skill = this.state.food.skill;
      this.state.foodEaten = 0;
    }
    else {
      this.state.foodEaten += 1;
    }
  }
};

Game.Engine.ElementsGenerator.prototype._getRandomicFood = function(notAvailableFoods) {
  notAvailableFoods = notAvailableFoods || [];

  this.state.food = false;

  while(!this.state.food) {
    this._generateSafeFood(notAvailableFoods);
  }
}

Game.Engine.ElementsGenerator.prototype._generateSafeFood = function(notAvailableFoods) {
  this._generateRandomicFoodType(notAvailableFoods);

  this._garanteeSafeFood(this.state.snake.getSnakePositions());

  if (this.state.map.blocks) {
    this._garanteeSafeFood(this.state.map.blocks);
  }
};

Game.Engine.ElementsGenerator.prototype._generateRandomicFoodType = function(notAvailableFoods) {
  var foodRange = Math.random();

  if (foodRange < 0.5) {
    this.state.food = new Game.Food.Basic();
  }
  else if (foodRange >= 0.5 && foodRange < 0.67) {
    this.state.food = new Game.Food.Speed();
  }
  else if (foodRange >= 0.67 && foodRange < 0.84) {
    this.state.food = new Game.Food.Slow();
  }
  else if (foodRange >= 0.84 && foodRange < 0.92 && $.inArray("magnetize", notAvailableFoods) == -1) {
    this.state.food = new Game.Food.Magnetize();
  }
  else if (foodRange >= 0.92 && $.inArray("shield", notAvailableFoods) == -1) {
    this.state.food = new Game.Food.Shield();
  }
  else if ($.inArray("uctt", notAvailableFoods) == -1) {
    this.state.food = new Game.Food.UCTT();
  }
  else {
    this.state.food = new Game.Food.Basic();
  }
};

Game.Engine.ElementsGenerator.prototype._garanteeSafeFood = function(possibleCollisions) {
  var distance, self = this;

  possibleCollisions.forEach(function(position) {
    if (self.state.food) {
      distance = position.distanceFrom(self.state.food.position);
      if (distance[0] == 0 && distance[1] == 0) {
        self.state.food = false;
      }
    }
  });
};

Game.Engine.ElementsGenerator.prototype._initializeFood = function() {
  this.state.food = new Game.Food.Basic();
};

Game.Engine.ElementsGenerator.prototype._initializeSnake = function() {
  this.state.snake = new Game.Movable.Snake();
};

Game.Engine.ElementsGenerator.prototype._initializeMap = function() {
  this.state.map = new Game.Map.Generator(0);
};

Game.Engine.ElementsGenerator.prototype._initializeSkill = function() {
  this.state.skill = new Game.Skills.Basic();
};