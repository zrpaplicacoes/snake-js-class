Game.Engine.CollisionsIdentifier = function() {
  this.possibleSolutions = ["breakTheBlock",
                            "gameOver",
                            "eatIt"]
}

Game.Engine.CollisionsIdentifier.prototype.identifyCollisions = function(collisions, gameState) {
  var identifiedCollisions = {};
  if (collisions["mapCollision"]) {
    if (gameState.breakableMap) {
      identifiedCollisions["breakTheBlock"] = collisions["mapCollision"];
    }
    else {
      identifiedCollisions["gameOver"] = collisions["mapCollision"];
    }
  }

  if (collisions["foodCollision"]) {
    identifiedCollisions["eatIt"] = collisions["foodCollision"];
  }

  if(collisions["snakeCollision"]) {
    identifiedCollisions["gameOver"] = collisions["snakeCollision"];
  }

  return identifiedCollisions;
};