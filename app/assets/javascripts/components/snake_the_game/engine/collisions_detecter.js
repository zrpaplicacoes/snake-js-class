Game.Engine.CollisionsDetecter = function() {
}

Game.Engine.CollisionsDetecter.prototype.findCollisions = function() {
  var collisions = {};

  collisions["mapCollision"] = this._detectMapCollisions();
  collisions["foodCollision"] = this._detectFoodCollisions();
  collisions["snakeCollision"] = this._detectSnakeCollisions();

  return collisions;
}

Game.Engine.CollisionsDetecter.prototype._detectMapCollisions = function() {
  return this._detectCollisionsBetween(".map", ".head");
};

Game.Engine.CollisionsDetecter.prototype._detectFoodCollisions = function() {
  return this._detectCollisionsBetween(".food", ".head");
};

Game.Engine.CollisionsDetecter.prototype._detectSnakeCollisions = function() {
  return this._detectCollisionsBetween(".tail", ".head") || this._detectCollisionsBetween(".body", ".head");
};

Game.Engine.CollisionsDetecter.prototype._detectCollisionsBetween = function(class1, class2) {
  var conflictIndex = Game.Executor.getWrapper().find(class1 + class2).index(),
      conflictPosition;

  if (conflictIndex >= 0) {
    conflictPosition = this._parseIndexAsPosition(conflictIndex);
  }
  else {
    conflictPosition = false;
  }

  return conflictPosition;
};

Game.Engine.CollisionsDetecter.prototype._parseIndexAsPosition = function(index) {
  var position = new Game.Position();

  position.set1DPosition(index);

  return position;
};