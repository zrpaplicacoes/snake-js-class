Game.Engine.Control = function() {
  this.state = {
    score: 0,
    level: 1,
    soundController: new Game.Sounds()
  };

  this.collisionsDetecter = new Game.Engine.CollisionsDetecter();
  this.collisionsIdentifier = new Game.Engine.CollisionsIdentifier();
  this.elementsGenerator = new Game.Engine.ElementsGenerator();
  this.levelController = new Game.Engine.LevelController();
};

Game.Engine.Control.prototype.init = function() {
  this.elementsGenerator.init();
  this.updateState(this.elementsGenerator.state);
};

Game.Engine.Control.prototype.build = function(newState) {
  if (!newState.paused) {
    this.state.soundController.updateState(newState);

    this.elementsGenerator.build({
      food: this.state.food,
      map: this.state.map,
      skill: this.state.skill,
      snake: this.state.snake,
      level: this.state.level,
      foodEaten: this.state.foodEaten,
      UCTTPassed: this.state.UCTTPassed,
      UCTTPlaying: this.state.UCTTPlaying,
      alreadyMagnetizedFood: this.state.alreadyMagnetizedFood
    });

    this.levelController.build({
      score: this.state.score,
      level: this.state.level,
      gameSpeed: this.state.gameSpeed,
      map: this.state.map,
      soundController: this.state.soundController
    });

    this.updateState(newState);
    this.run();
    this.updateState(this.elementsGenerator.state);
    this.updateState(this.levelController.state);
    this.state = this.state.skill.manipulateState(this.state);
  }
};

Game.Engine.Control.prototype.updateState = function(gameState) {
  updateState({
    state: this.state,
    newState: gameState
  });
}

Game.Engine.Control.prototype.run = function() {
  this._runState();
  this._moveElements();
};

Game.Engine.Control.prototype._moveElements = function() {
  this.state.snake.move();
};

Game.Engine.Control.prototype._runState = function() {
  var collisions = this.collisionsDetecter.findCollisions(),
      identifiedCollisions = this.collisionsIdentifier.identifyCollisions(collisions, this.state);

  for (var gameDecision in identifiedCollisions) {
    if (identifiedCollisions.hasOwnProperty(gameDecision)) {
      switch(gameDecision) {
        case "breakTheBlock":
          this._breakTheBlock(identifiedCollisions[gameDecision]);
        break;
        case "gameOver":
          this._endGame(identifiedCollisions[gameDecision]);
        break;
        case "eatIt":
          this._eatTheFood();
        break;
        default:
        break;
      }
    }
  }
};

Game.Engine.Control.prototype._breakTheBlock = function(blockPosition) {
  this.state.map.destroyBlock(blockPosition);
  this.state.soundController.breakMap();
};

Game.Engine.Control.prototype._endGame = function(collisionPosition) {
  this.state.gameOver = true;
  this.state.gameOverPosition = collisionPosition;
  this.state.soundController.gameOver();
};

Game.Engine.Control.prototype._eatTheFood = function() {
  this.state.snake.eat(1);
  this.elementsGenerator.updateSkillWithFood();
  this.elementsGenerator.generateNewFood();

  this.levelController.increaseScoreBy(this.state.food.punctuation);

  if (this.elementsGenerator.state.skill.skin != "uctt") {
    this.state.soundController.eatFood(this.elementsGenerator.state.skill.skin);
  }
};