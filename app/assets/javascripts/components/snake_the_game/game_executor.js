var GAMESIZE = 30,
    WRAPPER,
    GAMESPEED = 100;

Game.Executor = function($wrapper) {
  WRAPPER = $wrapper;
  this.state = {
    paused: false,
    gameboard: {
                  elements: $wrapper.find("span"),
                  size: GAMESIZE
                },
    gameSpeed: GAMESPEED,
    foodEaten: 0
  }
  this.gameTimeout;
  this.render = new Game.Render(this.state);
  this.engine = new Game.Engine.Control();
};

Game.Executor.prototype.init = function() {
  this.engine.init();
  this.state.level = 1;
};

Game.Executor.prototype.build = function(newState) {
  var self = this;

  this.updateState(newState);

  if (this.state.gameOver) {
    this.render.clearMaps();
    this._addGameScreenProtector();
    clearTimeout(this.gameTimeout);
  }
  else if (this.state.paused) {
    clearTimeout(this.gameTimeout);
  }
  else {
    this.gameTimeout = setTimeout(function() {
      self._runGame();
      self.build(self.state);
    }, this.state.gameSpeed);
  }
};

Game.Executor.prototype._runGame = function() {
  this.engine.build(this.state);
  this.updateState(this.engine.state);
  this.render.build(this.state);
};

Game.Executor.prototype.removeGameScreenProtector = function() {
  $(".js-overlay").fadeOut(400, function() {
    $(".js-start").removeClass("zrp-btn-start").addClass("zrp-btn-restart");
    $(".js-start").find("span").text("RECOMEÇAR");
  });
};

Game.Executor.prototype._addGameScreenProtector = function() {
  $(".js-overlay").fadeIn(500);
  $(".js-pause-control").fadeOut(0);
};

Game.Executor.prototype.updateState = function(gameState) {
  updateState({
    state: this.state,
    newState: gameState
  });

  GAMESPEED = this.state.gameSpeed;
}

Game.Executor.gameboardSize = function() {
  return GAMESIZE;
};

Game.Executor.getWrapper = function() {
  return WRAPPER;
};

Game.Executor.getPixels = function() {
  return WRAPPER.find('.pixel');
};

Game.Executor.getGameSpeed = function() {
  return GAMESPEED;
};