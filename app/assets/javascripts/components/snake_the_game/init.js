//= require_self
//= require ./game_executor
//= require ./position/init
//= require ./food/init
//= require ./skills/init
//= require ./movable/init
//= require ./render/init
//= require ./engine/init
//= require ./map/init
//= require ./sounds/init

var Game = {};

function updateState(options) {
  $.extend(options.state, options.newState);
}