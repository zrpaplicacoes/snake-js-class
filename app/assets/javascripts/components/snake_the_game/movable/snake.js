Game.Movable.Snake = function() {
  this.movimentController = new Game.Movable.MovimentController();
  this.movimentListener = new Game.Movable.MovimentsListener();
  this.state = {
    length: 3
  }

  this.movimentListener.bindListeners();
};

Game.Movable.Snake.prototype.eat = function(sizeOfTheFood) {
  this.state.length += sizeOfTheFood;
  this.movimentController.state.feedingBy = sizeOfTheFood;
};

Game.Movable.Snake.prototype.move = function() {
  this.movimentListener.updateKeyboardEvent();
  this.movimentController.move(this.movimentListener.state.lastDirectionSelected);
};

Game.Movable.Snake.prototype.render = function() {
  this.movimentController.render();
};

Game.Movable.Snake.prototype.getSnakePositions = function() {
  return this.movimentController.state.snakePosition;
};