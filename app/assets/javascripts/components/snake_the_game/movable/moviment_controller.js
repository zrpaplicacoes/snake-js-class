Game.Movable.MovimentController = function() {
  this.state = {
    currentDirection: "right",
    snakePosition: this._initialPosition(),
    feedingBy: 0
  }
};

Game.Movable.MovimentController.prototype.move = function(newDirection) {
  this.state.currentDirection = newDirection;
  switch(this.state.currentDirection) {
    case "right":
      this._moveTailToHead(1, 0);
    break;
    case "left":
      this._moveTailToHead(-1, 0);
    break;
    case "top":
      this._moveTailToHead(0, -1);
    break;
    case "bottom":
      this._moveTailToHead(0, 1);
    break;
    default:
    break;
  }
};

Game.Movable.MovimentController.prototype._moveTailToHead = function(offsetX, offsetY) {
  var head = this.state.snakePosition[0],
      tail = this._getTail(),
      newPosition = new Game.Position();

  newPosition.setPosition(head.state.x + offsetX, head.state.y + offsetY);
  tail.copyPosition(newPosition);
  tail.state.direction = this.state.currentDirection;

  this._fixNewTailDirection();

  this.state.snakePosition.unshift(tail);
};

Game.Movable.MovimentController.prototype.render = function() {
  var tail, self = this;

  Game.Executor.getPixels().removeClass("snake body head tail left right top bottom");

  this.state.snakePosition.forEach(function(position, index) {
    position.getElement().addClass("snake " + position.state.direction);

    if (index == self.state.snakePosition.length - 1) {
      position.getElement().addClass("tail");
    }
    else if (index == 0) {
      position.getElement().addClass("head");
    }
    else {
      position.getElement().addClass("body");
    }
  });
};

Game.Movable.MovimentController.prototype._getTail = function() {
  var tail;

  if (this.state.feedingBy > 0) {
    this.state.feedingBy -= 1;
    tail = new Game.Position();
    tail.copyPosition(this.state.snakePosition[this.state.snakePosition.length - 1]);
  }
  else {
    tail = this.state.snakePosition.pop();
  }

  return tail;
};

Game.Movable.MovimentController.prototype._fixNewTailDirection = function() {
  this.state.snakePosition[this.state.snakePosition.length - 1].state.direction = this.state.snakePosition[this.state.snakePosition.length - 2].state.direction;
};

Game.Movable.MovimentController.prototype._initialPosition = function() {
  var position1 = new Game.Position(),
      position2 = new Game.Position(),
      position3 = new Game.Position();

  position2.setPosition(position1.state.x + 1, position1.state.y);
  position3.setPosition(position1.state.x + 2, position1.state.y);
  return [position1, position2, position3];
};