Game.Movable.MovimentsListener = function() {
  this.state = {
    lastDirectionSelected: "left"
  }
};

Game.Movable.MovimentsListener.prototype.bindListeners = function() {
  this._bindKeyboardEvents();
  this.updateKeyboardEvent();
};

Game.Movable.MovimentsListener.prototype._bindKeyboardEvents = function() {
  var self = this;

  $(document).keydown(function(event){
    var selectedKey = event.which;

    if((selectedKey == 38 || selectedKey ==  87) && self.state.directionSelected != 'bottom') {
      event.preventDefault();
      self.state.lastDirectionSelected = "top";
    }
    else if((selectedKey == 37 || selectedKey == 65) && self.state.directionSelected != 'right') {
      event.preventDefault();
      self.state.lastDirectionSelected = "left";
    }
    else if((selectedKey == 39 || selectedKey == 68) && self.state.directionSelected != 'left') {
      event.preventDefault();
      self.state.lastDirectionSelected = "right";
    }
    else if((selectedKey == 40 || selectedKey == 83) && self.state.directionSelected != 'top') {
      event.preventDefault();
      self.state.lastDirectionSelected = "bottom";
    };
  });
};

Game.Movable.MovimentsListener.prototype.updateKeyboardEvent = function() {
  this.state.directionSelected = this.state.lastDirectionSelected;
};