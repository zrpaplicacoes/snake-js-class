Game.Render = function(gameState) {
  this.state = gameState;
};

Game.Render.prototype.build = function(newState) {
  this.updateState(newState);
  this.render();
};

Game.Render.prototype.updateState = function(gameState) {
  updateState({
    state: this.state,
    newState: gameState
  });
};

Game.Render.prototype.render = function() {
  if (!this.state.paused) {
    $(".snake").removeClass("dead");
    $(".js-uctt").removeClass('appear');
    this.state.food.render();
    this.state.snake.render();
    this.state.skill.render();
    this.state.map.render();

    this._renderSkillSkin();
    this._renderGameStatus();
  }
};

Game.Render.prototype._renderSkillSkin = function() {
  Game.Executor.getPixels().removeClass("magnetized unbreakable")
  $(".snake").addClass(this.state.skill.skin);
};

Game.Render.prototype._renderGameStatus = function() {
  if (this.state.gameOver) {
    $(".snake").addClass("dead");
  }
};

Game.Render.prototype.clearMaps = function() {
  Game.Executor.getPixels().removeClass("map");
};