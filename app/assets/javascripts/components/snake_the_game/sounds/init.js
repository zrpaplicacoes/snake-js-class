Game.Sounds = function($scope) {
	this.state = {};
	this.$scope = $scope || $('.js-sound');
};

Game.Sounds.prototype.updateState = function(newState) {
	this.state.muted = newState.muted || newState.UCTTPlaying;
};

Game.Sounds.prototype.eatFood = function(skin) {
	if (!this.state.muted) { $("[data-food-"+skin+"-sound]", this.$scope)[0].play(); }
};

Game.Sounds.prototype.levelRaise = function() {
	if (!this.state.muted) {
		$("[data-level-raise-sound]", this.$scope)[0].play();
		$("[data-generated-map-sound]", this.$scope)[0].play();
	}
};

Game.Sounds.prototype.breakMap = function() {
	if (!this.state.muted) { $("[data-break-map-sound]", this.$scope)[0].play();	}
};

Game.Sounds.prototype.gameOver = function() {
	if (!this.state.muted) { $("[data-game-over-sound]", this.$scope)[0].play();	}
};
