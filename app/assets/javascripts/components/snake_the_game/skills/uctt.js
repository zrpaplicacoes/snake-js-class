Game.Skills.UCTT = function() {
  this.skin = "uctt";
  this.state = {};
  this.maxTimeUCTT = Math.floor((Math.random() + 1) * 300);
};

Game.Skills.UCTT.prototype.manipulateState = function(gameState) {
  gameState.UCTTPassed = gameState.UCTTPassed || 0;
  gameState.UCTTPassed += 1;

  this.state.UCTTPassed = gameState.UCTTPassed;

  gameState.food.position = this._getUCTTFoodPosition(gameState);
  gameState.UCTTPlaying = true;

  $(".js-music-control").removeClass("fa-volume-up").addClass("fa-volume-off muted");
  $("[data-game-sound]")[0].pause();

  if (this.endedUptime()) {
    gameState.food = new Game.Food.Basic();
    gameState.skill = new Game.Skills.Basic();
    gameState.UCTTPlaying = false;
  }
  return gameState;
};

Game.Skills.UCTT.prototype.render = function() {
  if (this.endedUptime()) {
    $(".js-uctt").removeClass('appear');
    $("[data-uctt]")[0].pause();
  }
  else {
    $(".js-uctt").addClass('appear');
    $("[data-uctt]")[0].play();
  }
};

Game.Skills.UCTT.prototype.endedUptime = function() {
  return this.state.UCTTPassed >= this.maxTimeUCTT;
};

Game.Skills.UCTT.prototype._getUCTTFoodPosition = function(gameState) {
  var newPosition = new Game.Position(),
      headPosition = gameState.snake.getSnakePositions()[0],
      offset = [];

  switch(headPosition.state.direction) {
    case "top":
      offset = [0, -1];
    break;

    case "bottom":
      offset = [0, 1];
    break;

    case "left":
      offset = [-1, 0];
    break;

    case "right":
      offset = [1, 0];
    break;
  }

  newPosition.setPosition(headPosition.state.x + offset[0], headPosition.state.y + offset[1]);

  return newPosition;
};