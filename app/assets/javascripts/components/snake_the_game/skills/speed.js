Game.Skills.Speed = function() {
  this.skin = "speeded";
  this.addedEffect = false;
};

Game.Skills.Speed.prototype.manipulateState = function(gameState) {
  if (this._canAddEffect(gameState)) {
    gameState.gameSpeed -= 10;
    this.addedEffect = true;
  }
  return gameState;
};

Game.Skills.Speed.prototype.render = function() {
};

Game.Skills.Speed.prototype._canAddEffect = function(gameState) {
  return !this.addedEffect && gameState.gameSpeed > 10;
};