Game.Skills.Shield = function() {
  this.skin = "unbreakable";
  this.state = {
    activationCount: 0
  }
};

Game.Skills.Shield.prototype.manipulateState = function(gameState) {
  if (this.endedUptime()) {
    gameState.breakableMap = false;
    gameState.skill = new Game.Skills.Basic();
  }
  else if (!gameState.paused) {
    gameState.breakableMap = true;
    this.state.activationCount += 1;
  }

  return gameState;
};

Game.Skills.Shield.prototype.render = function() {
  if (this.endedUptime()) {
    $(".snake").removeClass(this.skin);
  }
  else {
    $(".snake").addClass(this.skin);
  }
};

Game.Skills.Shield.prototype.endedUptime = function() {
  return this.state.activationCount > (40 * Math.sqrt(2));
};