Game.Skills.Slow = function() {
  this.skin = "slowed";
  this.addedEffect = false;
};

Game.Skills.Slow.prototype.manipulateState = function(gameState) {
  if (this._canAddEffect(gameState)) {
    gameState.gameSpeed += 10;
    this.addedEffect = true;
  }
  return gameState;
};

Game.Skills.Slow.prototype.render = function() {
};

Game.Skills.Slow.prototype._canAddEffect = function(gameState) {
  return !this.addedEffect && gameState.gameSpeed < 120;
;
};