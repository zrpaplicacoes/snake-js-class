Game.Skills.Magnetize = function() {
  this.skin = "magnetized";
  this.state = {};
  this.maxFoodEaten = Math.floor((Math.random() * 10) + 1);
};

Game.Skills.Magnetize.prototype.manipulateState = function(gameState) {
  this.state.foodEaten = gameState.foodEaten;

  if (!gameState.alreadyMagnetizedFood) {
    gameState.alreadyMagnetizedFood = true;
    gameState.food.position = this._getMagnetizedFoodPosition(gameState);
  }

  return gameState;
};

Game.Skills.Magnetize.prototype.render = function() {
  if (this.endedUptime()) {
    $(".snake").removeClass(this.skin);
  }
  else {
    $(".snake").addClass(this.skin);
  }
};

Game.Skills.Magnetize.prototype.endedUptime = function() {
  return this.state.foodEaten >= this.maxFoodEaten;
};

Game.Skills.Magnetize.prototype._getMagnetizedFoodPosition = function(gameState) {
  var newPosition = new Game.Position(),
      headPosition = gameState.snake.getSnakePositions()[0],
      offset = [];

  switch(headPosition.state.direction) {
    case "top":
      offset = [0, -1];
    break;

    case "bottom":
      offset = [0, 1];
    break;

    case "left":
      offset = [-1, 0];
    break;

    case "right":
      offset = [1, 0];
    break;
  }

  newPosition.setPosition(headPosition.state.x + offset[0], headPosition.state.y + offset[1]);

  return newPosition;
};