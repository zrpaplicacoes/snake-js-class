Game.Food.Magnetize = function() {
  this.skin = "magnetize";
  this.punctuation = 80;
  this.state = { 'lifetime' : 0 };
  this.skill = new Game.Skills.Magnetize();
  this.position = getRandomicPosition();
};

Game.Food.Magnetize.prototype.uptime = function(timeBase) {
  return timeBase / Math.sqrt(3);
};

Game.Food.Magnetize.prototype.updateState = function(newState) {
  updateState({
    state: this.state,
    newState: newState
  });
};

Game.Food.Magnetize.prototype.render = function() {
  renderFood(this);
};

Game.Food.Magnetize.prototype.endedUptime = function() {
  return endedUptime(this);
};
