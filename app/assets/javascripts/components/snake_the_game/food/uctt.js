Game.Food.UCTT = function() {
  this.skin = "uctt";
  this.punctuation = 888;
  this.state = { 'lifetime' : 0 };
  this.skill = new Game.Skills.UCTT();
  this.position = getRandomicPosition();
};

Game.Food.UCTT.prototype.uptime = function(timeBase) {
  return timeBase / Math.sqrt(3);
};

Game.Food.UCTT.prototype.updateState = function(newState) {
  updateState({
    state: this.state,
    newState: newState
  });
};

Game.Food.UCTT.prototype.render = function() {
  renderFood(this);
};

Game.Food.UCTT.prototype.endedUptime = function() {
  return endedUptime(this);
};
