Game.Food.Speed = function() {
  this.skin = "speed";
  this.punctuation = 40;
  this.state = { 'lifetime' : 0 };
  this.skill = new Game.Skills.Speed();
  this.position = getRandomicPosition();
};

Game.Food.Speed.prototype.uptime = function(timeBase) {
  return timeBase;
};

Game.Food.Speed.prototype.updateState = function(newState) {
  updateState({
    state: this.state,
    newState: newState
  });
};

Game.Food.Speed.prototype.render = function() {
  renderFood(this);
};

Game.Food.Speed.prototype.endedUptime = function() {
  return endedUptime(this);
};
