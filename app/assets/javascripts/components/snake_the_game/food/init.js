//= require_self
//= require ./basic
//= require ./magnetize
//= require ./shield
//= require ./slow
//= require ./speed
//= require ./uctt

Game.Food = {};

function endedUptime(food) {
  return food.uptime(Game.Executor.getGameSpeed()) < food.state.lifetime;
};

function renderFood(food) {
  Game.Executor.getPixels().removeClass('food magnetize shield speed slow uctt');

  if (!food.endedUptime()) {
    food.position.getElement().addClass('food ' + food.skin);
    food.state.lifetime += 1;
  }
};

function getRandomicPosition() {
  return new Game.Position();
}