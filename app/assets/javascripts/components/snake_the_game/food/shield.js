Game.Food.Shield = function() {
  this.skin = "shield";
  this.punctuation = 100;
  this.state = { 'lifetime' : 0 };
  this.skill = new Game.Skills.Shield();
  this.position = getRandomicPosition();
};

Game.Food.Shield.prototype.uptime = function(timeBase) {
  return timeBase / Math.sqrt(2);
};

Game.Food.Shield.prototype.updateState = function(newState) {
  updateState({
    state: this.state,
    newState: newState
  });
};

Game.Food.Shield.prototype.render = function() {
  renderFood(this);
};

Game.Food.Shield.prototype.endedUptime = function() {
  return endedUptime(this);
};
