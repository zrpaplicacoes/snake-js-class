Game.Food.Basic = function() {
  this.skin = "basic";
  this.punctuation = 20;
  this.state = { 'lifetime' : 0 };
  this.skill = new Game.Skills.Basic();
  this.position = getRandomicPosition();
};

Game.Food.Basic.prototype.uptime = function(timeBase) {
  return timeBase * 2;
};

Game.Food.Basic.prototype.updateState = function(newState) {
  updateState({
    state: this.state,
    newState: newState
  });
};

Game.Food.Basic.prototype.render = function() {
  renderFood(this);
};

Game.Food.Basic.prototype.endedUptime = function() {
  return endedUptime(this);
};
