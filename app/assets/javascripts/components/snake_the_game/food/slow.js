Game.Food.Slow = function() {
  this.skin = "slow";
  this.punctuation = 40;
  this.state = { 'lifetime' : 0 };
  this.skill = new Game.Skills.Slow();
  this.position = getRandomicPosition();
};

Game.Food.Slow.prototype.uptime = function(timeBase) {
  return timeBase;
};

Game.Food.Slow.prototype.updateState = function(newState) {
  updateState({
    state: this.state,
    newState: newState
  });
};

Game.Food.Slow.prototype.render = function() {
  renderFood(this);
};

Game.Food.Slow.prototype.endedUptime = function() {
  return endedUptime(this);
};
