Game.Map.Renderer = function(){};

Game.Map.Renderer.prototype.render = function(blocks) {
  Game.Executor.getPixels().removeClass("map");

  blocks.forEach(function(block) {
    block.getElement().addClass("map");
  });
};