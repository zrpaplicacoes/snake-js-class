Game.Map.Generator = function(level) {
  this.state = {
    level: level,
    blocks: []
  };
  this.renderer = new Game.Map.Renderer();
};

Game.Map.Generator.prototype.setLevel = function(level) {
  this.state.level = level;
  this._generateMapForLevel();
};

Game.Map.Generator.prototype.getBlackListedPositions = function() {
  return this.state.blocks;
};

Game.Map.Generator.prototype.render = function() {
  this.renderer.render(this.state.blocks);
  Game.Executor.getWrapper().find(".map.food").removeClass("map");
  Game.Executor.getWrapper().find(".map.snake.body").removeClass("map");
};

Game.Map.Generator.prototype.destroyBlock = function(block) {
  var index, self = this, distance;

  this.state.blocks.forEach(function(existingBlock) {
    distance = existingBlock.distanceFrom(block);
    if (distance[0] == 0 &&
        distance[1] == 0) {
      index = self.state.blocks.indexOf(existingBlock);
      self.state.blocks.splice(index, 1);
    }
  });
};

Game.Map.Generator.prototype._generateMapForLevel = function() {
  var setOfBlocks = Math.floor(Math.random() * 5 + 1) * this.state.level;
  while(setOfBlocks > 0) {
    this._addBlocks();
    setOfBlocks--;
  }
};

Game.Map.Generator.prototype._addBlocks = function() {
  var numberOfBlocks = Math.floor(Math.random() * 3 + 1),
      newBlock = this._addANotConflictingBlock();

  switch(numberOfBlocks) {
    case 2:
      this._addSiblingBlock(newBlock, 0, -1);
    break;
    case 3:
      this._addSiblingBlock(newBlock, 0, -1);
      this._addSiblingBlock(newBlock, 1, -1);
    break;
    case 4:
      this._addSiblingBlock(newBlock, 0, -1);
      this._addSiblingBlock(newBlock, 1, -1);
      this._addSiblingBlock(newBlock, 1, 0);
    break;
    default:
    break;
  }
};

Game.Map.Generator.prototype._addSiblingBlock = function(position, offsetX, offsetY) {
  var newPosition = new Game.Position();

  newPosition.setPosition(position.state.x + offsetX, position.state.y + offsetY);
  this.state.blocks.push(newPosition);
};

Game.Map.Generator.prototype._addANotConflictingBlock = function() {
  var blockAdded = false,
      newBlock = new Game.Position();

  while (!blockAdded) {
    if (this._canIAddThisBlock(newBlock)) {
      this.state.blocks.push(newBlock);
      blockAdded = true;
    }
    else {
      newBlock = new Game.Position();
    }
  }

  return newBlock;
};

Game.Map.Generator.prototype._canIAddThisBlock = function(block) {
  var distance, validBlock = true;

  this.state.blocks.forEach(function(existingBlock) {
    distance = existingBlock.distanceFrom(block);
    if (Math.abs(distance[0]) < 3 && Math.abs(distance[1]) < 3) {
      validBlock = false;
    }
  });

  return validBlock;
};