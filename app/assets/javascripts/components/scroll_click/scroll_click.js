Components.ScrollClick = function(identifier) {
  this.identifier = identifier;
}

Components.ScrollClick.prototype.build = function() {
  var self = this;
  $("body").on("click", this.identifier, function(event) {
    event.preventDefault();
    var destination = $(self.identifier).data('redirect');
    $("html, body").stop().animate({
      scrollTop: $(destination).offset().top,
    }, 500);
  });
};