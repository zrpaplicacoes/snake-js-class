module ApplicationHelper
	def current_namespace
		controller_downcase.split("::").first
	end

	def current_ncv
		"#{controller_downcase.split("::").join("_")}_#{action_name}"
	end

	private

	def controller_downcase
		controller.class.name.downcase.gsub("controller", "")
	end
end
