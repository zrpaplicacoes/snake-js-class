class ErrorNotifier
  attr_accessor :exception, :code

  def initialize options
    @exception = options[:exception]
    @code = options[:code]
    @notifier = setup_notifier
  end

  def notify_404
    if Rails.env.production?
      @notifier.ping ":scream: Error type #{@code} :scream:"
      @notifier.ping ":triumph: *Page not found: #{@exception}*"
    end
  end

  def notify_500
    if Rails.env.production?
      attachment = attach_backtrace
      @notifier.ping ":scream: Error type #{@code} :scream:"
      @notifier.ping ":triumph: *#{exception.message}*"
      @notifier.ping backtrace_title, attachments: [attachment]
    end
  end

  private

  def setup_notifier
    notifier = Slack::Notifier.new ENV['SLACK_NOTIFICATION_WEBHOOK_URL']
    notifier.channel = ENV['SLACK_NOTIFICATION_WEBHOOK_CHANNEL']
    notifier
  end

  def attach_backtrace
    {
      fallback: backtrace_title,
      text:  @exception.backtrace.join("\n"),
      color: "danger"
    }
  end

  def backtrace_title
    "Error backtrace log - #{DateTime.current}"
  end
end