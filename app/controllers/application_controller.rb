require "application_responder"
require "slack-notifier"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Personalizated errors handler
  unless Rails.application.config.consider_all_requests_local
    rescue_from Exception do |exception|
      render_internal_error(exception)
    end
    rescue_from ActionController::RoutingError do |exception|
      render_not_found(exception)
    end
  end

  def raise_not_found!
    raise ActionController::RoutingError.new(params[:unmatched_route])
  end

  def render_not_found exception
    error_notifier = ErrorNotifier.new({code: 404, exception: exception})
    error_notifier.notify_404

    respond_to do |format|
      format.any{ render template: "errors/not_found", layout: 'errors/main', content_type: 'text/html', formats: [:html], status: 404 }
    end
  end

  def render_internal_error exception
    error_notifier = ErrorNotifier.new({code: 404, exception: exception})
    error_notifier.notify_500

    respond_to do |format|
      format.any{ render template: "errors/internal_error", layout: 'errors/main', content_type: 'text/html', formats: [:html], status: 500 }
    end
  end
end
