var gulp = require('gulp'),
    babel = require('gulp-babel'),
    concat = require('gulp-concat'),
    commonjsWrap = require('gulp-wrap-commonjs'),
    uglify = require('gulp-uglify');

var paths = {
    es6PathComponents: 'app/assets/javascripts/components/**/*.es6',
    es6PathViews: 'app/assets/javascripts/views/**/*.es6',
    compiledPathComponents: 'app/assets/javascripts/compiled_javascripts/components',
    compiledPathViews: 'app/assets/javascripts/compiled_javascripts/views',
};


gulp.task('babel:views', function() {
  return gulp.src([paths.es6PathViews])
      .pipe(babel({modules: 'common', moduleIds: true, moduleRoot: ''}))
      .pipe(
          commonjsWrap({
            autoRequire: false,
            pathModifier: function (path) {
              path = path.replace(/.js$/, '')
                      .replace(/^.*\/views/, 'views')

              return path;
            }
          })
      )
      .pipe(uglify())
      .pipe(gulp.dest(paths.compiledPathViews));
});

gulp.task('babel:components', function() {
  return gulp.src([paths.es6PathComponents])
      .pipe(babel({modules: 'common', moduleIds: true, moduleRoot: ''}))
      .pipe(
          commonjsWrap({
            autoRequire: false,
            pathModifier: function (path) {
              path = path.replace(/.js$/, '')
                      .replace(/^.*\/components/, 'components')

              return path;
            }
          })
      )
      .pipe(uglify())
      .pipe(gulp.dest(paths.compiledPathComponents));
});

gulp.task('js:compiler', function() {
  gulp.watch([paths.es6PathComponents], ['babel:components']);
  gulp.watch([paths.es6PathViews], ['babel:views']);
});

gulp.task('default', ['js:compiler', 'babel:components', 'babel:views']);
