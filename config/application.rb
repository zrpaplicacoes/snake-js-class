require File.expand_path('../boot', __FILE__)

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "action_controller/railtie"
require "action_view/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RailsSkeleton
  class Application < Rails::Application
    # Use the responders controller from the responders gem
    config.app_generators.scaffold_controller :responders_controller

    config.autoload_paths += %W(#{config.root}/lib)
    if Rails.env.production?
      config.middleware.insert_before ActionDispatch::Static, Rack::Deflater
      config.middleware.insert_before ActionDispatch::Static, Rack::Cors do
        allow do
          origins '*'
          resource '/assets/*',
          headers: :any,
          methods: [:get, :options]
        end
      end
    end

    config.time_zone = 'Brasilia'

    #I18n configs
    config.i18n.locale = :"pt-BR"
    config.i18n.default_locale = :"pt-BR"
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    #Compile configs

    # Precompile inits from css
    config.assets.precompile += [ "init.css" ]

    # Precompile inits from js
    config.assets.precompile += [ "init.js" ]

    config.assets.precompile << /\.(?:svg|eot|woff|woff2|ttf)$/
    config.assets.precompile += %w[ *.png *.jpeg *.jpg *.gif *.mp4 *.ogg ]
    config.assets.paths << "#{Rails.root}/vendor/assets/fonts"
    config.assets.paths << "#{Rails.root}/vendor/assets/images"

    #Config de helpers
    config.action_controller.include_all_helpers = false
  end
end
